==========
GMの心得
==========

.. note:: 突然、鈴の音が聞こえ、頭に花飾りをつけた妖精らしきものが現れた。

   さて冒険の開始だ！　……とその前に、忘れ物がないかちゃんと確かめたかな？

   ここではGMがセッションを行う上でのアドバイスを載せていくよ。

   最初に覚えるべき大切なルールももう一度確認して、GMについての理解を含めて理解してほしい。


セッションとは
================
ElonaTRPGで遊ぶ1回分、話の区切りのことをセッションと呼びます。参加者が集まって遊び始めるときがセッション開始、遊び終えたときがセッション終了というわけです。

この説明だけでは『セッション』のイメージが湧かないならば、マンガや小説、連続ドラマなど、なんでも好きな物を思い浮かべてみてください。一話完結のもの、次回へ続くもの、いろいろありますが、この一話分が1セッションにあたります。


セッションのはじめから終わりまで
==================================

セッションの準備
------------------
GMはゲームをスムーズに始められるよう、セッション当日までにいくつかの準備を行わなければなりません。以下の6つのどれが欠けても、セッションの進行は難しくなってしまうでしょう。

シナリオ
   セッション1回分のシナリオを作っておく必要があります。

   いくら魅力的なキャラクターを作ったところで、何もわからないまま「ご自由に」と言われてもプレイヤー(PL)は戸惑ってしまうでしょう。彼らのキャラクターが今どこにいるのか、今は昼なのか、それとも夜なのか、そしてどんな事件が起きているのか。プレイヤーが行動するために必要な舞台を作らなければなりません。

   シナリオ作りについては、後の :doc:`scenario/index` に書かれています。

日程調整
   1回のセッションにはおよそ3〜6時間かかります。参加者同士でうまく予定を合わせるために、調整が欠かせません。

参加者募集
   PLの人数は3～5人程度をおすすめします。無理なくプレイングの処理ができる人数が良いでしょう。。

場所の確保
   プレイする場所も必要です。

   オフラインセッションの場合は公共施設の一室を借りたり、誰かの家におじゃましたりといった方法が考えられます。誰が利用費の負担をするか、誰かの家を利用する場合は場所提供者へのお礼をどうするかなど、事前準備は欠かせません。

   オンラインセッションの場合も、参加者各自がネットワークに接続できるかどうか、各自がセッションに使うWebサービスやソフトの使い方を把握しているかは確認しておくべきでしょう。

ルールブックの理解
   GMはある程度ルールや世界観を把握しておくことが望ましいです。完璧に全てのルールを覚える必要はありませんが、わからないことが出てきたときにルールブックのどこを調べれば良いか覚えておくと、セッションの進行がスムーズとなります。
プレイヤーキャラクター(PC)の作成
   PLには :doc:`../data/making-your-character/index` に則ってPLの分身であるPCを作成してもらうことになります。

   集まってから作成する場合も、あらかじめ参加者同士で打ち合わせて作ってくる場合もありますが、そのどちらなのかGMはPLに知らせておかなければなりません。

   なお、PCの作成にはおよそ30分～2時間程度の時間がかかります。


.. ここまでやった

セッションの運営
-------------------

ここではGMがセッションを実際に行うにあたっての手順などをあげていきます。

手順

1.自己紹介
  まず自己紹介から始めましょう、GMは今回のセッションや雰囲気も先にPLに伝えておくといいでしょう。PLも自身のキャラクターやPL自身について自己紹介しておくとお互いをしれてスムーズに進むでしょう。またこの時にPCの大体のできることを把握したりメモったりしましょう。

2.セッションの開始
  自己紹介がすんだらGMのセッションの開始の宣言とともにGMはPLにPCたちが現在いる状態や風景などの描写を伝えるといいでしょう。

3.導入
  次にGMはシナリオの導入とPCの行動を促すといいでしょう。このときのシナリオの導入は「依頼人に依頼を頼まれる」「急に何者かに襲われる」「ヒロインが助けを求めてくる」などさまざまです。

4.本編
  導入終了後、GMはPLに自由に発言、行動させるといいでしょう。GMはそれを聞きシナリオを進行させます。

  PLが動くのに困ったときはいろいろ選択肢を出し、導くのもGMの役目です。しかしできるだけPLの主体性に任せましょう。

5.クライマックス
  ネフィアのボスや隠れてる敵を追い詰めたときなどの戦闘などはセッション中で盛り上がる場面になるでしょう。ただし絶対に戦闘が必要というわけではありません、GMつまりあなたが盛り上がると確信できる演出やギミックを登場させれるならばそれがクライマックスです。

  また緊張感のあるクライマックスを作る場合は*模擬戦などは欠かせないでしょう。

6.エンディング
  無事、依頼や問題が片付いた場合セッションは終了です。エンディングやエピローグなどを挟むといいでしょう。あと報酬などの処理もこの時に渡すといいでしょう。

脚注）模擬戦：模擬戦闘のこと、どの程度の難易度でクリアできるかを図るために(実際のPLのPCやサンプルキャラクターで)先頭を行うこと。

セッションの終わり
--------------------
GMがセッションの終了を宣言したらセッションは終了です。後片付けをし次も気持ちよくセッションができるようにしましょう。もし許すなら終わった後に食事や雑談をしてどこがよかったかなどを話すと次のモチベーションに繋がります。ただし反省会はしないことをお勧めします。

そしてセッションの始まりに戻ります。次回までにPLはキャラクターの成長、GMはシナリオの準備などをしましょう。

注意点
-------------------
  ここではセッション中によく起こる問題について説明します。

・PLはGMのシナリオの本筋から沿わないような行動を行った。
  できるだけやんわりと本筋に沿うよう誘導しましょう。あまりにもGMの妨害だと思われる行動をする場合はこれ以上参加させないという決断をすることもGMには認められています。

  ただし、想定から離れるような展開になっても、セッションが盛り上がり尚且つあなたが面白いと判断するならシナリオを変更してもよいです。

  GMはPLの前では堂々とPLに接しましょう。

・プレイヤーの質問に答える
  セッション中にPLはGMにさまざまな疑問や質問を投げかけます。GMはできるだけPLにその質問に答えましょう。特にPLやPCが置かれている状況などには詳しく説明するとPLが悩まずにすみセッションの停滞を防ぎます。

  教えることでセッションの盛り上がりがなくなるというもの（敵データや真相など）以外にはできるだけ答えることが望まれます。

・イメージを共有する
  TRPGは会話で進めていくゲームのため言葉のみだとどうしても勘違いや齟齬が出ます。それを防ぐためGMはできるだけ丁寧に描写を行ったりイラストや音楽、動画などを用意するといいでしょう。

・セッションに集中させる
  会話するゲームという性質上雑談などでセッションの進行が滞る場合があります。ですがこのような｢ながらプレイ｣はセッションの進行を阻害するだけではなくその楽しみを大きく減退させます。GMはメリハリをつけ無理やり打ち切ったり時間などを決めてやるといいでしょう。

  また長時間のセッションはとても疲労します。GMは1時間に1回程度10分～15分程度の休憩をとるといいでしょう。

