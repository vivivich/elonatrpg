==============
GMセクション
==============

.. note:: 王立図書館・閉架の扉

   **『許可無き者の立入りを禁ず』**

.. toctree::
   :maxdepth: 2

   rules-for-game-master
   scenario/index
   bestiary/index
   npcs/index

