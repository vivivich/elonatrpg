====================
データ・セクション
====================

.. note:: 異形の森の使者の言葉

   **イルヴァについて深く知れば、それだけ君の旅の安全は増すだろう。**

   **尤も、それほど深くの知識を要する運命を君が背負っているならばの話だが。**

.. toctree::
   :maxdepth: 2

   making-your-character/index
   character-data/index
   rules
   battle/index
   magic/index
   items/index
   tables/index

