アイテム
==========

.. toctree::

   weapons
   armors
   others
   magical-canes
   scrolls
   usable-items
   pets
