====================
キャラクターの能力
====================

.. todo:: MPの基礎値と振分ポイント上昇分を分けて書かないと、魔力上昇時に面倒くさいことになる。

.. toctree::

   main-attributes
   sub-attributes
   skills
   magic
   feats
