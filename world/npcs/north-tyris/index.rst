=====================
ノースティリスのNPC
=====================

パルミア
----------

.. toctree::

   xabi
   stersha
   conery
   erystia
   mia

ヴェルニース
--------------

.. toctree::

   miches
   shena
   loyter
   vanity
   rilian
   poppy
   dungeon-cleaner

ポート・カプール
------------------

.. toctree::

   icolle
   raphael
   doria
   arnord
   fray

ヨウィン
----------

.. toctree::

   tam
   gwen
   gilbert
   ainc

ダルフィ
----------

.. toctree::

   noel
   marks
   abyss
   sin

ルミエスト
------------

.. toctree::

   balzak
   renton
   lexus
   revlus

ノイエル
----------

.. toctree::

   moyer
   ebon
   pael
   lily

アクリ・テオラ
----------------

.. toctree::

   scientist

工房ミラル・ガロク
--------------------

.. toctree::

   miral
   garok

その他
--------

.. toctree::

   lomias
   larnneire
   saimore
   varius
   sebilis
   karam
   slan
   norne
