========================
ElonaTRPG ルールブック
========================

.. attention::

   このルールブックは製作途中のものです。

   ご意見、ご感想、罵倒、投石などはtwitter(@ ElonaTRPG_ )によろしくお願い致します。

   最終更新 |today|

もくじ
========

.. toctree::
   :maxdepth: 2

   getting-started
   data/index
   world/index
   gm/index


検索
======

.. * :ref:`genindex`

* :ref:`search`

.. _ElonaTRPG: https://twitter.com/ElonaTRPG
